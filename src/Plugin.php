<?php

namespace FWP\WebPush;

class Plugin
{

    public function run()
    {
        $this->registerClientScript();
    }

    protected function registerClientScript()
    {
        add_action('wp_enqueue_scripts', function() {
            wp_register_script('wp-web-push-firebase', 'https://www.gstatic.com/firebasejs/4.1.3/firebase.js');
//            wp_register_script('wp-web-push-main', '/wp-content/plugins/wp-web-push/js/main.js');
            wp_enqueue_script('wp-web-push-firebase');
//            wp_enqueue_script('wp-web-push-main');
        });
    }

}
