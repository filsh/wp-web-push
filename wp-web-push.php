<?php
/*
Plugin Name: WP Web Push
Version: 2.6.2
*/
use FWP\WebPush\Plugin;
use FWP\WebPush\Installer;

register_activation_hook(__FILE__, [Installer::class, 'onActivate']);
register_deactivation_hook(__FILE__, [Installer::class, 'onDeactivate']);
register_uninstall_hook(__FILE__, [Installer::class, 'onUninstall']);

(new Plugin())->run();
