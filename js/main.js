import { compose, curry } from 'ramda'
import config from './config'
import messaging from './messaging'
import registerWorker from 'serviceworker-loader!./sw.js'

const handleError = err => console.log(err)

const handleToken = (token) => {
  console.log(token)
}

const requestToken = (config) => registerWorker()
  .then(worker => {
    const instance = messaging(config)
    instance.useServiceWorker(worker)
    return instance.requestPermission().then(() => instance.getToken())
  })

const handleRequest = (handleToken, handleError, promise) => promise
  .then(handleToken)
  .catch(handleError)

const composeWebPush = compose(
  curry(handleRequest)(handleToken, handleError),
  requestToken
)

export default () => composeWebPush({ messagingSenderId: config.messagingSenderId })
