import config from './config'
import messaging from './messaging'

messaging({
  messagingSenderId: config.messagingSenderId
})
