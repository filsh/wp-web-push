import * as firebase from 'firebase/app'
import 'firebase/messaging'

export default (config) => firebase.initializeApp(config).messaging()
